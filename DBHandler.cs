﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace CRMGURU
{
    //Базовый класс для работы с базами данных
    public abstract class DbHandler
    {
        //Проверяет значение в бд. Generic потому, что, если в дальнейшем работать с int
        //или другими типами б.данных.
        public bool ValueInDB<T>(string value, List<object> dataList)
        {
            T rowValue = (T)Convert.ChangeType(value, typeof(T));

            foreach (object item in dataList)
            {
                if (item is T dataListValue)
                {
                    dataListValue = (T)item;
                    if (EqualityComparer<T>.Default.Equals(dataListValue, rowValue))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        
        public abstract void ConnectToDB();
        public abstract void CreateCmd(CommandType cmdType = CommandType.Text);
        public abstract void ExecuteNonQuery(string query);
        public abstract int FillAdapter(ref DataTable data);
        public abstract void OpenConnection();
        public abstract void CloseConnection();

        public abstract List<object> ReadData(string query);
        public abstract void ReadData(string query, ComboBox cb);
    }
}
