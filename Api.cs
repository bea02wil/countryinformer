﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace CRMGURU
{
    //Enum для разных методов http. Предположим, что api работает
    //не только с методом Get, как указано на их главной странице.
    enum RequestMethods
    {
        GET
        //...
    }

    //Enum по какому сервису искать
    enum Service
    {
        all,
        name,
        capital
        //....
    }

    class Api
    {
        private string requestString;

        public Api()
        {
            requestString = "https://restcountries.eu/rest/v2/";       
        }

        public Country[] SearchCountries(string userInput, string requestFilter)
        {
            string requestUriString = GetUriString(Service.name, userInput, requestFilter);

            return Get<Country>(requestUriString);
        }

        //Generic для того, что, если предположим, что api включает в себя не только страны,
        //а еще что-то
        private T[] Get<T>(string requestUriString)
        {          
            string responseString = Request(RequestMethods.GET, requestUriString);
            object endPoint = JsonConvert.DeserializeObject<T[]>(responseString);
                      
            return (T[])Convert.ChangeType(endPoint, typeof(T[]));
        }

        private string Request(RequestMethods method, string requestUri)
        {
            string responseString = string.Empty;

            var request = WebRequest.CreateHttp(requestUri);

            request.Method = method.ToString();

            using (var response = request.GetResponse())
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        responseString = reader.ReadToEnd();
                    }
                }
            }

            return responseString;
        }

        private string GetUriString(Service service, string serviceName, string requestFilter)
        {          
            return requestString + service.ToString() + @"/" + serviceName + requestFilter;           
        }
    }    
}
