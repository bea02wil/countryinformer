﻿using MaterialSkin;
using MaterialSkin.Controls;

namespace CRMGURU
{
    static class FormDesign
    {
        public static void SetDesign(MaterialForm form)
        {
            MaterialSkinManager formDesign = MaterialSkinManager.Instance;
            formDesign.AddFormToManage(form);
            formDesign.Theme = MaterialSkinManager.Themes.DARK;
            formDesign.ColorScheme = new ColorScheme(Primary.Brown800, Primary.Brown500,
                Primary.Blue300, Accent.LightBlue400, TextShade.WHITE);
        }
    }
}
