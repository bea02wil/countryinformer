﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;

namespace CRMGURU
{
    //Реализация для работы с бд Oracle 
    class OracleDB : DbHandler
    {
        OracleConnection connection;

        public OracleCommand Cmd { get; set; }

        public OracleDB()
        {
            ConnectToDB();
        }

        public override void ConnectToDB()
        {
            try
            {
                ConnectionStringSettings conString;
                conString = ConfigurationManager.ConnectionStrings["CRMGURUDataBase"];

                connection = new OracleConnection(conString.ConnectionString);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public override void CreateCmd(CommandType cmdType = CommandType.Text)
        {
            Cmd = connection.CreateCommand();
            Cmd.CommandType = cmdType;
        }

        public override void ExecuteNonQuery(string query)
        {
            Cmd.CommandText = query;
            Cmd.ExecuteNonQuery();
        }

        public override int FillAdapter(ref DataTable data)
        {
            data.Columns.Clear();
            data.Clear();

            OracleDataAdapter dataAdapter = new OracleDataAdapter(Cmd);
            int adapterFlag = dataAdapter.Fill(data);
            return adapterFlag;
        }

        public override void OpenConnection() => connection.Open();

        public override void CloseConnection() => connection.Close();

        public override List<object> ReadData(string query)
        {
            ExecuteNonQuery(query);
            List<object> dataList = new List<object>();

            using (OracleDataReader reader = Cmd.ExecuteReader())
            {
                int columnsNumber = reader.FieldCount;
                while (reader.Read())
                {
                    for (int i = 0; i < columnsNumber; i++)
                    {
                        object rowValue = reader.GetValue(i);
                        dataList.Add(rowValue);
                    }
                }
                reader.Close();
            }
            return dataList;
        }

        public override void ReadData(string query, ComboBox cb)
        {
            ExecuteNonQuery(SqlQuery.SelectRegionsName());
            using (OracleDataReader reader = Cmd.ExecuteReader())
            {
                int columnsNumber = reader.FieldCount;
                while (reader.Read())
                {
                    for (int i = 0; i < columnsNumber; i++)
                    {
                        string rowValue = reader.GetString(i);
                        cb.Items.Add(rowValue);
                    }
                }
                reader.Close();
                cb.Sorted = true;
            }
        }
    }
}
