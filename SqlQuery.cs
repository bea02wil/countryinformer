﻿
namespace CRMGURU
{
    static class SqlQuery
    {
        //С формы CountryInformer. Запросы SELECT
        #region CountryInformerForm
        
        public static string DisplayAllCountries()
        {
            string allCountries = "SELECT Countries.Name, Countries.Code, Cities.Name AS Capital, " +
               "Countries.Area, Countries.Population, Regions.Name AS Region FROM[Countries] " +
               "LEFT JOIN [Cities] ON Cities.Id = Countries.Capital " +
               "LEFT JOIN [Regions] ON Regions.Id = Countries.Region";
            return allCountries;
        }
        #endregion

        // С формы AddData
        #region AddDataForm
        //Запросы UPDATE    
        public static string UpdateCitiesName(string countryCapital, string countryCode)
        {
            string updatedCities = "UPDATE [Cities] SET Name = '" + countryCapital + "'" +
                    " WHERE Id = (SELECT Capital FROM [Countries] WHERE Code = '" + countryCode + "')";
            return updatedCities;
        }

        public static string UpdateCountriesName(string countryName, float countryArea,
            int countryPop, string countryRegion, string countryCode)
        {
            string updatedCountries = "UPDATE [Countries] SET Name = '" + countryName + "'," +
                    " Area = '" + countryArea + "', Population = '" + countryPop + "'," +
                    " Region = (SELECT Id FROM [Regions] WHERE Name = '" + countryRegion + "')" +
                    " WHERE Code = '" + countryCode + "'";
            return updatedCountries;
        }

        //Запросы SELECT
        public static string SelectRegionsName()
        {
            string allRegionsNames = "SELECT Name FROM [Regions]";
            return allRegionsNames;
        }

        public static string SelectCountriesCode()
        {
            string allCountriesCodes = "SELECT Code FROM [Countries]";
            return allCountriesCodes;
        }

        //Запросы INSERT
        public static string InsertToRegionsName(string countryRegion)
        {
            string insertedRegions = "INSERT INTO [Regions] (Name) VALUES ('" + countryRegion + "')";         
            return insertedRegions;
        }

        public static string InsertToCitiesName(string countryCapital)
        {
            string insertedCities = "INSERT INTO [Cities] (Name) VALUES ('" + countryCapital + "')";
            return insertedCities;
        }

        public static string InsertToCountriesFields(string countryName, string countryCode,
            float countryArea, int countryPop, string countryRegion)
        {
            string insertedFields = "INSERT INTO [Countries] (Name,Code,Capital,Area,Population,Region) VALUES" +
                " ('" + countryName + "','" + countryCode + "'," +
                "(SELECT SCOPE_IDENTITY()),'" + countryArea + "','" + countryPop + "'," +
                " (SELECT Id FROM [Regions] WHERE Name = '" + countryRegion + "'))";
            return insertedFields;
        }
        #endregion
    }
}
