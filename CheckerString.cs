﻿using System.Text.RegularExpressions;

namespace CRMGURU
{    
    static class CheckerString
    {      
        public static bool StringCorrect(string name, string pattern)
        {
            return Regex.IsMatch(name, pattern);
        }
    }
}
