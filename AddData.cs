﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MaterialSkin.Controls;

namespace CRMGURU
{
    public partial class AddData : MaterialForm
    {            
        string countryName;       
        DbHandler dBHandler;

        public AddData(string countryName, DbHandler dBHandler)
        {
            InitializeComponent();
            this.countryName = countryName;
            this.dBHandler = dBHandler;         
            txtCountryNameAD.Text = FirstLetterToUpper(countryName);       
            UpdateCbCountryRegion(SqlQuery.SelectRegionsName());
            FormDesign.SetDesign(this);
        }

        #region btnClick events
        private void btnSaveAD_Click(object sender, EventArgs e)
        {
            if (!FieldsCorrect())
            {
                MessageBox.Show("Fix errors and try again!", "Field input errors.",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            AllowEmptyStrings(out float countryArea, out int countryPop);
            
            if (IsCodeExists(lblCode.Text))
            {
                UpdateAllData(countryArea, countryPop);              
                return;
            }
           
            try
            {
                InsertAllData(countryArea, countryPop);               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            finally
            {
                dBHandler.CloseConnection();
            }

            MessageBox.Show("Data has been successfully added!", "Success!",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }
     
        private void btnCloseAD_Click(object sender, EventArgs e)
        {
            try
            {
                dBHandler.CloseConnection();
            }
            catch (SqlException sqe)
            {
                MessageBox.Show(sqe.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Close();
        }
        #endregion

        #region Text fields leave events
        private void txtCountryNameAD_Leave(object sender, EventArgs e)
        {
            string countryName = txtCountryNameAD.Text;
            string message = "No digits!";
          
            CheckFieldValidation(countryName, StringPatterns.NoDigits(), message,
                lblNameCountry, lblNameCountryCol, txtCountryNameAD);
        }

        private void txtCountryCodeAD_Leave(object sender, EventArgs e)
        {         
            string countryCode = txtCountryCodeAD.Text;
            string message1 = "Only digits!";
            string message2 = "message will be updated";
                      
            CheckFieldValidation(countryCode, StringPatterns.DigitsOnly(), message1,
                lblCode2, lblCodeCol, txtCountryCodeAD ,message2);
        }

        private void txtCountryCapitalAD_Leave(object sender, EventArgs e)
        {
            string countryCapital = txtCountryCapitalAD.Text;
            string message = "No digits!";
           
            CheckFieldValidation(countryCapital, StringPatterns.NoDigits(), message,
                lblCapital, lblCapitalCol, txtCountryCapitalAD);
        }

        private void txtCountryAreaAD_Leave(object sender, EventArgs e)
        {
            string countryArea = txtCountryAreaAD.Text;
            string message = "Only digits!";
           
            CheckFieldValidation(countryArea, StringPatterns.DigitsOnly(), message,
                lblArea, lblAreaCol, txtCountryAreaAD);
        }

        private void txtCountryPopAD_Leave(object sender, EventArgs e)
        {
            string countryPop = txtCountryPopAD.Text;
            string message = "Only digits!";
           
            CheckFieldValidation(countryPop, StringPatterns.DigitsOnly(), message,
                lblPopulation, lblPopCol, txtCountryPopAD);
        }
    
        private void cbCountryRegion_Leave(object sender, EventArgs e)
        {
            string countryRegion = cbCountryRegion.Text;
            string message = "Only digits!";
          
            CheckFieldValidation(countryRegion, StringPatterns.NoDigits(), message,
                lblRegion, lblRegionCol, cbCountryRegion);
        }

        #endregion

        //Проверяет label поля на пустую строку. Если пустая, значит, надо исправить ввод данных в форме.
        private bool FieldsCorrect()
        {
            string[] fields = new string[] {lblNameCountry.Text, lblCode2.Text,
                lblCapital.Text, lblArea.Text, lblPopulation.Text, lblRegion.Text };
            foreach (string field in fields)
            {
                if (!string.IsNullOrEmpty(field))
                {
                    return false;
                }              
            }
            return true;
        }

        //Проверяет сами поля ввода данных и обновляет статус меток, исходя из проверки первого.
        private void CheckFieldValidation(string field, string stringPattern, string message,
            Label lblName, MaterialLabel lblNameCol, object fieldType, string message2 = "")
        {
            if (!string.IsNullOrEmpty(field))
            {
                if (!CheckerString.StringCorrect(field, StringPatterns.EnglishString(false)))
                {
                    if (lblName.Name != "lblCode2" && lblName.Name != "lblArea"
                        && lblName.Name != "lblPopulation")
                    {
                        if (lblName.Name != "lblRegion")
                        {
                            string messageEnglish = "English letters no spaces";
                            FieldMessage(messageEnglish, lblName, lblNameCol, Color.Red);
                        }
                        else
                        {
                            if (!CheckerString.StringCorrect(field, StringPatterns.EnglishString(true)))
                            {
                                string messageEnglish = "English letters only!";
                                FieldMessage(messageEnglish, lblName, lblNameCol, Color.Red);
                            }
                        }
                    }
                    else
                    {                  
                        string messageDigits = "Only digits!";
                        FieldMessage(messageDigits, lblName, lblNameCol, Color.Red);
                    }
                }

                else if (!CheckerString.StringCorrect(field, stringPattern))
                {                  
                    FieldMessage(message, lblName, lblNameCol, Color.Red);
                }            

                else
                {
                    if (stringPattern == StringPatterns.NoDigits())                   
                        field = TranslateFirstLetterToUpper(field, fieldType);
                    
                    if (!string.IsNullOrEmpty(message2))
                    {
                        message2 = "The country for this code exists in the DB. Data will be updated";
                        try
                        {
                            CheckFieldInDB<string>(field, SqlQuery.SelectCountriesCode(), lblCode, message2);
                        }
                        catch (Exception ex)
                        {
                            DisplayErrorMsg(ex);
                        }
                        FieldMessage(string.Empty, lblCode2, lblCodeCol, Color.White);
                        return;
                    }

                    FieldMessage(string.Empty, lblName, lblNameCol, Color.White);
                }
            }
            else
            {
                if (lblName.Name == "lblNameCountry" || lblName.Name == "lblCode2")
                    FieldMessage("Required field", lblName, lblNameCol, Color.Red);
          
                else FieldMessage(string.Empty, lblName, lblNameCol, Color.White);
            }
        }

        private string TranslateFirstLetterToUpper(string field, object fieldType)
        {
            field = FirstLetterToUpper(field);
            if (fieldType is MaterialSingleLineTextField M)
            {
                M = (MaterialSingleLineTextField)fieldType;
                M.Text = field;
            }
            else if (fieldType is ComboBox C)
            {
                C = (ComboBox)fieldType;
                C.Text = field;
            }

            return field;
        }

        //Выдает статус сообщение проверки поля в метку.
        private void FieldMessage(string message, Label lblName, MaterialLabel matLblName, Color lblsColor)
        {
            lblName.ForeColor = Color.Red;
            matLblName.ForeColor = lblsColor;

            lblName.Text = message;         
        }

        //Проверят статус у первой метки кода. Если оно не пустое, значит обновляем данные. 
        private bool IsCodeExists(string field)
        {           
            if (!string.IsNullOrEmpty(field)) return true;
            else return false;
        }

        //Обновляет данные базы данных.
        private void UpdateAllData(float countryArea, int countryPop)
        {
            try
            {
                CheckFieldInDB<string>(cbCountryRegion.Text, SqlQuery.SelectRegionsName(),
                            SqlQuery.InsertToRegionsName(cbCountryRegion.Text));

                dBHandler.ExecuteNonQuery(SqlQuery.UpdateCitiesName(txtCountryCapitalAD.Text, txtCountryCodeAD.Text));

                dBHandler.ExecuteNonQuery(SqlQuery.UpdateCountriesName(txtCountryNameAD.Text, countryArea, countryPop,
                    cbCountryRegion.Text, txtCountryCodeAD.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            finally
            {
                dBHandler.CloseConnection();
            }
            MessageBox.Show("Data has been successfully updated!", "Success!",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
           
            this.Close();
        }

        //Вставляет данные в базу данных.
        private void InsertAllData(float countryArea, int countryPop)
        {
            //Вставляет не найденный введенный регион. Например, North America, Melanesia, South Africa и прочее.
            CheckFieldInDB<string>(cbCountryRegion.Text, SqlQuery.SelectRegionsName(),
                            SqlQuery.InsertToRegionsName(cbCountryRegion.Text));

            dBHandler.ExecuteNonQuery(SqlQuery.InsertToCitiesName(txtCountryCapitalAD.Text));

            dBHandler.ExecuteNonQuery(SqlQuery.InsertToCountriesFields(txtCountryNameAD.Text, txtCountryCodeAD.Text,
                countryArea, countryPop, cbCountryRegion.Text));
        }

        //Возвращает заглавную первую букву строки.
        private string FirstLetterToUpper(string name)
        {
            return name.First().ToString().ToUpper() + name.Substring(1);
        }

        //Проверяет значение поля в бд и выводит статус в метку, если оно есть в бд.
        //Generic для поддержки разных типов атрибутов бд.
        //В данном случае используется для проверки поля Code.
        private void CheckFieldInDB<T>(string fieldName, string selectQuery, Label field, string message)
        {
            List<object> dataList = dBHandler.ReadData(selectQuery);
            if (dBHandler.ValueInDB<T>(fieldName, dataList))
            {
                field.ForeColor = Color.Orange;
                field.Text = message;
            }          

            else field.Text = string.Empty;
        }

        //Проверяет значение поля в бд и выполняет соответствующий запрос, если поле найдено.
        //....
        //В данном случае используется для проверки регионов.
        private void CheckFieldInDB<T>(string fieldName, string selectQuery, string insertQuery)
        {
            if (!string.IsNullOrEmpty(fieldName))
            {              
                List<object> dataList = dBHandler.ReadData(selectQuery);
                if (dBHandler.ValueInDB<T>(fieldName, dataList)) return;

                else dBHandler.ExecuteNonQuery(insertQuery);
            }
        }

        //Допускает значения нуль для сл. полей, если эти поля пустые (тип этих полей в базе int и float).
        private void AllowEmptyStrings(out float countryArea, out int countryPop)
        {
            countryArea = string.IsNullOrEmpty(txtCountryAreaAD.Text) ? 0 
                : float.Parse(txtCountryAreaAD.Text);

            countryPop = string.IsNullOrEmpty(txtCountryPopAD.Text) ? 0 
                : int.Parse(txtCountryPopAD.Text);         
        }

        //Обновляет ComboBox регионов
        private void UpdateCbCountryRegion(string query)
        {
            try
            {              
                cbCountryRegion.Items.Clear();
                dBHandler.ReadData(query, cbCountryRegion);
            }           
            catch (Exception ex)
            {
                DisplayErrorMsg(ex);
            }
        }
      
        private void DisplayErrorMsg(Exception ex)
        {
            MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            dBHandler.CloseConnection();
            this.Close();
        }      
    }
}
