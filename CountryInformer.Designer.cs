﻿namespace CRMGURU
{
    partial class CountryInformer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCountryName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.btnCountryList = new MaterialSkin.Controls.MaterialFlatButton();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.btnFindCountry = new MaterialSkin.Controls.MaterialFlatButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // txtCountryName
            // 
            this.txtCountryName.Depth = 0;
            this.txtCountryName.Hint = "";
            this.txtCountryName.Location = new System.Drawing.Point(139, 147);
            this.txtCountryName.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCountryName.Name = "txtCountryName";
            this.txtCountryName.PasswordChar = '\0';
            this.txtCountryName.SelectedText = "";
            this.txtCountryName.SelectionLength = 0;
            this.txtCountryName.SelectionStart = 0;
            this.txtCountryName.Size = new System.Drawing.Size(223, 23);
            this.txtCountryName.TabIndex = 1;
            this.txtCountryName.UseSystemPasswordChar = false;          
            // 
            // btnCountryList
            // 
            this.btnCountryList.AutoSize = true;
            this.btnCountryList.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCountryList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCountryList.Depth = 0;
            this.btnCountryList.Location = new System.Drawing.Point(686, 199);
            this.btnCountryList.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnCountryList.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCountryList.Name = "btnCountryList";
            this.btnCountryList.Primary = false;
            this.btnCountryList.Size = new System.Drawing.Size(79, 36);
            this.btnCountryList.TabIndex = 3;
            this.btnCountryList.Text = "Show all";
            this.btnCountryList.UseVisualStyleBackColor = true;
            this.btnCountryList.Click += new System.EventHandler(this.btnCountryList_Click);
            // 
            // dataGridView
            // 
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Location = new System.Drawing.Point(26, 255);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersWidth = 122;
            this.dataGridView.Size = new System.Drawing.Size(743, 186);
            this.dataGridView.TabIndex = 4;
            // 
            // btnFindCountry
            // 
            this.btnFindCountry.AutoSize = true;
            this.btnFindCountry.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnFindCountry.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFindCountry.Depth = 0;
            this.btnFindCountry.Location = new System.Drawing.Point(395, 134);
            this.btnFindCountry.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnFindCountry.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnFindCountry.Name = "btnFindCountry";
            this.btnFindCountry.Primary = false;
            this.btnFindCountry.Size = new System.Drawing.Size(42, 36);
            this.btnFindCountry.TabIndex = 5;
            this.btnFindCountry.Text = "Find";
            this.btnFindCountry.UseVisualStyleBackColor = true;
            this.btnFindCountry.Click += new System.EventHandler(this.btnFindCountry_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(21, 147);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 22);
            this.label1.TabIndex = 6;
            this.label1.Text = "Country:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(253, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(286, 26);
            this.label2.TabIndex = 7;
            this.label2.Text = "Enter a country or display all";
            // 
            // CountryInformer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(782, 490);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnFindCountry);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.btnCountryList);
            this.Controls.Add(this.txtCountryName);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(782, 490);
            this.MinimumSize = new System.Drawing.Size(782, 490);
            this.Name = "CountryInformer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CountryInformer";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MaterialSkin.Controls.MaterialSingleLineTextField txtCountryName;
        private MaterialSkin.Controls.MaterialFlatButton btnCountryList;
        private System.Windows.Forms.DataGridView dataGridView;
        private MaterialSkin.Controls.MaterialFlatButton btnFindCountry;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

