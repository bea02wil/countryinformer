﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace CRMGURU
{
    class LocalDB : DbHandler
    {
        SqlConnection connection;

        public SqlCommand Cmd { get; set; }       

        public LocalDB()
        {
            ConnectToDB();           
        }

        public override void ConnectToDB()
        {
            try
            {
                ConnectionStringSettings conString;
                conString = ConfigurationManager.ConnectionStrings["CRMGURUDataBase"];

                connection = new SqlConnection(conString.ConnectionString);              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public override void CreateCmd(CommandType cmdType = CommandType.Text)
        {
            Cmd = connection.CreateCommand();
            Cmd.CommandType = cmdType;
        }

        public override void ExecuteNonQuery(string query)
        {
            Cmd.CommandText = query;
            Cmd.ExecuteNonQuery();
        }

        //Заполняет адаптер - мост связи между приложением и бд. 
        public override int FillAdapter(ref DataTable data)
        {
            data.Columns.Clear();
            data.Clear();

            SqlDataAdapter dataAdapter = new SqlDataAdapter(Cmd);           
            int adapterFlag = dataAdapter.Fill(data);          
            return adapterFlag;
        }

        public override void OpenConnection() => connection.Open();

        public override void CloseConnection() => connection.Close();

        //Читает данные с таблицы и заполняет список для следующих операций
        public override List<object> ReadData(string query)
        {
            ExecuteNonQuery(query);
            List<object> dataList = new List<object>();

            using (SqlDataReader reader = Cmd.ExecuteReader())
            {
                int columnsNumber = reader.FieldCount;
                while (reader.Read())
                {
                    for (int i = 0; i < columnsNumber; i++)
                    {
                        object rowValue = reader.GetValue(i);
                        dataList.Add(rowValue);
                    }
                }
                reader.Close();
            }
            return dataList;
        }

        //Читает данные с таблицы и заполняет combobox регионов
        public override void ReadData(string query, ComboBox cb)
        {
            ExecuteNonQuery(SqlQuery.SelectRegionsName());
            using (SqlDataReader reader = Cmd.ExecuteReader())
            {
                int columnsNumber = reader.FieldCount;
                while (reader.Read())
                {
                    for (int i = 0; i < columnsNumber; i++)
                    {
                        string rowValue = reader.GetString(i);
                        cb.Items.Add(rowValue);
                    }
                }
                reader.Close();
                cb.Sorted = true;
            }
        }
    }
}
