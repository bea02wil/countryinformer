﻿
namespace CRMGURU
{
    //Класс для сопоставления параметров с json ответом api
    public class Country
    {
        public float Area { get; set; }
        public string Capital { get; set; }
        public string Name { get; set; }
        public string NumericCode { get; set; }
        public int Population { get; set; }
        public string Region { get; set; }
        //...
    }
}
