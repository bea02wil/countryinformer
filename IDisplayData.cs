﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRMGURU
{
    //Отображает данные. Например, можно отобразить в dataGrid, файл и т.п.
    interface IDisplayData
    {
        void DisplayData();
    }
}
