﻿namespace CRMGURU
{
    partial class AddData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNameCountryCol = new MaterialSkin.Controls.MaterialLabel();
            this.txtCountryNameAD = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lblCodeCol = new MaterialSkin.Controls.MaterialLabel();
            this.txtCountryCodeAD = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lblCapitalCol = new MaterialSkin.Controls.MaterialLabel();
            this.txtCountryCapitalAD = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lblAreaCol = new MaterialSkin.Controls.MaterialLabel();
            this.txtCountryAreaAD = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lblPopCol = new MaterialSkin.Controls.MaterialLabel();
            this.txtCountryPopAD = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lblRegionCol = new MaterialSkin.Controls.MaterialLabel();
            this.btnSaveAD = new MaterialSkin.Controls.MaterialFlatButton();
            this.btnCloseAD = new MaterialSkin.Controls.MaterialFlatButton();
            this.cbCountryRegion = new System.Windows.Forms.ComboBox();
            this.lblNameCountry = new System.Windows.Forms.Label();
            this.lblCode = new System.Windows.Forms.Label();
            this.lblCapital = new System.Windows.Forms.Label();
            this.lblArea = new System.Windows.Forms.Label();
            this.lblPopulation = new System.Windows.Forms.Label();
            this.lblRegion = new System.Windows.Forms.Label();
            this.lblCode2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblNameCountryCol
            // 
            this.lblNameCountryCol.AutoSize = true;
            this.lblNameCountryCol.Depth = 0;
            this.lblNameCountryCol.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblNameCountryCol.ForeColor = System.Drawing.Color.Black;
            this.lblNameCountryCol.Location = new System.Drawing.Point(22, 92);
            this.lblNameCountryCol.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblNameCountryCol.Name = "lblNameCountryCol";
            this.lblNameCountryCol.Size = new System.Drawing.Size(49, 19);
            this.lblNameCountryCol.TabIndex = 0;
            this.lblNameCountryCol.Text = "Name";
            // 
            // txtCountryNameAD
            // 
            this.txtCountryNameAD.Depth = 0;
            this.txtCountryNameAD.Hint = "";
            this.txtCountryNameAD.Location = new System.Drawing.Point(125, 88);
            this.txtCountryNameAD.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCountryNameAD.Name = "txtCountryNameAD";
            this.txtCountryNameAD.PasswordChar = '\0';
            this.txtCountryNameAD.SelectedText = "";
            this.txtCountryNameAD.SelectionLength = 0;
            this.txtCountryNameAD.SelectionStart = 0;
            this.txtCountryNameAD.Size = new System.Drawing.Size(163, 23);
            this.txtCountryNameAD.TabIndex = 1;
            this.txtCountryNameAD.UseSystemPasswordChar = false;
            this.txtCountryNameAD.Leave += new System.EventHandler(this.txtCountryNameAD_Leave);
            // 
            // lblCodeCol
            // 
            this.lblCodeCol.AutoSize = true;
            this.lblCodeCol.Depth = 0;
            this.lblCodeCol.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblCodeCol.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblCodeCol.Location = new System.Drawing.Point(22, 141);
            this.lblCodeCol.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblCodeCol.Name = "lblCodeCol";
            this.lblCodeCol.Size = new System.Drawing.Size(44, 19);
            this.lblCodeCol.TabIndex = 2;
            this.lblCodeCol.Text = "Code";
            // 
            // txtCountryCodeAD
            // 
            this.txtCountryCodeAD.Depth = 0;
            this.txtCountryCodeAD.Hint = "";
            this.txtCountryCodeAD.Location = new System.Drawing.Point(125, 137);
            this.txtCountryCodeAD.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCountryCodeAD.Name = "txtCountryCodeAD";
            this.txtCountryCodeAD.PasswordChar = '\0';
            this.txtCountryCodeAD.SelectedText = "";
            this.txtCountryCodeAD.SelectionLength = 0;
            this.txtCountryCodeAD.SelectionStart = 0;
            this.txtCountryCodeAD.Size = new System.Drawing.Size(163, 23);
            this.txtCountryCodeAD.TabIndex = 3;
            this.txtCountryCodeAD.UseSystemPasswordChar = false;
            this.txtCountryCodeAD.Leave += new System.EventHandler(this.txtCountryCodeAD_Leave);
            // 
            // lblCapitalCol
            // 
            this.lblCapitalCol.AutoSize = true;
            this.lblCapitalCol.Depth = 0;
            this.lblCapitalCol.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblCapitalCol.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblCapitalCol.Location = new System.Drawing.Point(22, 188);
            this.lblCapitalCol.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblCapitalCol.Name = "lblCapitalCol";
            this.lblCapitalCol.Size = new System.Drawing.Size(56, 19);
            this.lblCapitalCol.TabIndex = 4;
            this.lblCapitalCol.Text = "Capital";
            // 
            // txtCountryCapitalAD
            // 
            this.txtCountryCapitalAD.Depth = 0;
            this.txtCountryCapitalAD.Hint = "";
            this.txtCountryCapitalAD.Location = new System.Drawing.Point(125, 184);
            this.txtCountryCapitalAD.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCountryCapitalAD.Name = "txtCountryCapitalAD";
            this.txtCountryCapitalAD.PasswordChar = '\0';
            this.txtCountryCapitalAD.SelectedText = "";
            this.txtCountryCapitalAD.SelectionLength = 0;
            this.txtCountryCapitalAD.SelectionStart = 0;
            this.txtCountryCapitalAD.Size = new System.Drawing.Size(163, 23);
            this.txtCountryCapitalAD.TabIndex = 5;
            this.txtCountryCapitalAD.UseSystemPasswordChar = false;
            this.txtCountryCapitalAD.Leave += new System.EventHandler(this.txtCountryCapitalAD_Leave);
            // 
            // lblAreaCol
            // 
            this.lblAreaCol.AutoSize = true;
            this.lblAreaCol.Depth = 0;
            this.lblAreaCol.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblAreaCol.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblAreaCol.Location = new System.Drawing.Point(22, 236);
            this.lblAreaCol.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblAreaCol.Name = "lblAreaCol";
            this.lblAreaCol.Size = new System.Drawing.Size(40, 19);
            this.lblAreaCol.TabIndex = 6;
            this.lblAreaCol.Text = "Area";
            // 
            // txtCountryAreaAD
            // 
            this.txtCountryAreaAD.Depth = 0;
            this.txtCountryAreaAD.Hint = "";
            this.txtCountryAreaAD.Location = new System.Drawing.Point(125, 232);
            this.txtCountryAreaAD.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCountryAreaAD.Name = "txtCountryAreaAD";
            this.txtCountryAreaAD.PasswordChar = '\0';
            this.txtCountryAreaAD.SelectedText = "";
            this.txtCountryAreaAD.SelectionLength = 0;
            this.txtCountryAreaAD.SelectionStart = 0;
            this.txtCountryAreaAD.Size = new System.Drawing.Size(163, 23);
            this.txtCountryAreaAD.TabIndex = 7;
            this.txtCountryAreaAD.UseSystemPasswordChar = false;
            this.txtCountryAreaAD.Leave += new System.EventHandler(this.txtCountryAreaAD_Leave);
            // 
            // lblPopCol
            // 
            this.lblPopCol.AutoSize = true;
            this.lblPopCol.Depth = 0;
            this.lblPopCol.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblPopCol.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblPopCol.Location = new System.Drawing.Point(22, 285);
            this.lblPopCol.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblPopCol.Name = "lblPopCol";
            this.lblPopCol.Size = new System.Drawing.Size(81, 19);
            this.lblPopCol.TabIndex = 8;
            this.lblPopCol.Text = "Population";
            // 
            // txtCountryPopAD
            // 
            this.txtCountryPopAD.Depth = 0;
            this.txtCountryPopAD.Hint = "";
            this.txtCountryPopAD.Location = new System.Drawing.Point(125, 281);
            this.txtCountryPopAD.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtCountryPopAD.Name = "txtCountryPopAD";
            this.txtCountryPopAD.PasswordChar = '\0';
            this.txtCountryPopAD.SelectedText = "";
            this.txtCountryPopAD.SelectionLength = 0;
            this.txtCountryPopAD.SelectionStart = 0;
            this.txtCountryPopAD.Size = new System.Drawing.Size(163, 23);
            this.txtCountryPopAD.TabIndex = 9;
            this.txtCountryPopAD.UseSystemPasswordChar = false;
            this.txtCountryPopAD.Leave += new System.EventHandler(this.txtCountryPopAD_Leave);
            // 
            // lblRegionCol
            // 
            this.lblRegionCol.AutoSize = true;
            this.lblRegionCol.Depth = 0;
            this.lblRegionCol.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblRegionCol.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblRegionCol.Location = new System.Drawing.Point(22, 335);
            this.lblRegionCol.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblRegionCol.Name = "lblRegionCol";
            this.lblRegionCol.Size = new System.Drawing.Size(55, 19);
            this.lblRegionCol.TabIndex = 10;
            this.lblRegionCol.Text = "Region";
            // 
            // btnSaveAD
            // 
            this.btnSaveAD.AutoSize = true;
            this.btnSaveAD.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSaveAD.Depth = 0;
            this.btnSaveAD.Location = new System.Drawing.Point(26, 385);
            this.btnSaveAD.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnSaveAD.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSaveAD.Name = "btnSaveAD";
            this.btnSaveAD.Primary = false;
            this.btnSaveAD.Size = new System.Drawing.Size(46, 36);
            this.btnSaveAD.TabIndex = 12;
            this.btnSaveAD.Text = "Save";
            this.btnSaveAD.UseVisualStyleBackColor = true;
            this.btnSaveAD.Click += new System.EventHandler(this.btnSaveAD_Click);
            // 
            // btnCloseAD
            // 
            this.btnCloseAD.AutoSize = true;
            this.btnCloseAD.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCloseAD.Depth = 0;
            this.btnCloseAD.Location = new System.Drawing.Point(234, 385);
            this.btnCloseAD.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btnCloseAD.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCloseAD.Name = "btnCloseAD";
            this.btnCloseAD.Primary = false;
            this.btnCloseAD.Size = new System.Drawing.Size(54, 36);
            this.btnCloseAD.TabIndex = 13;
            this.btnCloseAD.Text = "Close";
            this.btnCloseAD.UseVisualStyleBackColor = true;
            this.btnCloseAD.Click += new System.EventHandler(this.btnCloseAD_Click);
            // 
            // cbCountryRegion
            // 
            this.cbCountryRegion.BackColor = System.Drawing.SystemColors.MenuBar;
            this.cbCountryRegion.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbCountryRegion.FormattingEnabled = true;
            this.cbCountryRegion.Items.AddRange(new object[] {
            "Australia",
            "Asia",
            "Africa",
            "Europe",
            "Oceania",
            "Central America",
            "South America"});
            this.cbCountryRegion.Location = new System.Drawing.Point(125, 328);
            this.cbCountryRegion.MaxDropDownItems = 12;
            this.cbCountryRegion.Name = "cbCountryRegion";
            this.cbCountryRegion.Size = new System.Drawing.Size(163, 26);
            this.cbCountryRegion.TabIndex = 14;
            this.cbCountryRegion.Leave += new System.EventHandler(this.cbCountryRegion_Leave);
            // 
            // lblNameCountry
            // 
            this.lblNameCountry.AutoSize = true;
            this.lblNameCountry.Location = new System.Drawing.Point(180, 115);
            this.lblNameCountry.Name = "lblNameCountry";
            this.lblNameCountry.Size = new System.Drawing.Size(0, 13);
            this.lblNameCountry.TabIndex = 15;
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(8, 67);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(0, 13);
            this.lblCode.TabIndex = 16;
            // 
            // lblCapital
            // 
            this.lblCapital.AutoSize = true;
            this.lblCapital.Location = new System.Drawing.Point(182, 213);
            this.lblCapital.Name = "lblCapital";
            this.lblCapital.Size = new System.Drawing.Size(0, 13);
            this.lblCapital.TabIndex = 17;
            // 
            // lblArea
            // 
            this.lblArea.AutoSize = true;
            this.lblArea.Location = new System.Drawing.Point(180, 265);
            this.lblArea.Name = "lblArea";
            this.lblArea.Size = new System.Drawing.Size(0, 13);
            this.lblArea.TabIndex = 18;
            // 
            // lblPopulation
            // 
            this.lblPopulation.AutoSize = true;
            this.lblPopulation.Location = new System.Drawing.Point(180, 311);
            this.lblPopulation.Name = "lblPopulation";
            this.lblPopulation.Size = new System.Drawing.Size(0, 13);
            this.lblPopulation.TabIndex = 19;
            // 
            // lblRegion
            // 
            this.lblRegion.AutoSize = true;
            this.lblRegion.Location = new System.Drawing.Point(180, 357);
            this.lblRegion.Name = "lblRegion";
            this.lblRegion.Size = new System.Drawing.Size(0, 13);
            this.lblRegion.TabIndex = 20;
            // 
            // lblCode2
            // 
            this.lblCode2.AutoSize = true;
            this.lblCode2.Location = new System.Drawing.Point(180, 165);
            this.lblCode2.Name = "lblCode2";
            this.lblCode2.Size = new System.Drawing.Size(0, 13);
            this.lblCode2.TabIndex = 21;
            // 
            // AddData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 450);
            this.Controls.Add(this.lblCode2);
            this.Controls.Add(this.lblRegion);
            this.Controls.Add(this.lblPopulation);
            this.Controls.Add(this.lblArea);
            this.Controls.Add(this.lblCapital);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.lblNameCountry);
            this.Controls.Add(this.cbCountryRegion);
            this.Controls.Add(this.btnCloseAD);
            this.Controls.Add(this.btnSaveAD);
            this.Controls.Add(this.lblRegionCol);
            this.Controls.Add(this.txtCountryPopAD);
            this.Controls.Add(this.lblPopCol);
            this.Controls.Add(this.txtCountryAreaAD);
            this.Controls.Add(this.lblAreaCol);
            this.Controls.Add(this.txtCountryCapitalAD);
            this.Controls.Add(this.lblCapitalCol);
            this.Controls.Add(this.txtCountryCodeAD);
            this.Controls.Add(this.lblCodeCol);
            this.Controls.Add(this.txtCountryNameAD);
            this.Controls.Add(this.lblNameCountryCol);
            this.MaximizeBox = false;
            this.Name = "AddData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add data";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel lblNameCountryCol;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtCountryNameAD;
        private MaterialSkin.Controls.MaterialLabel lblCodeCol;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtCountryCodeAD;
        private MaterialSkin.Controls.MaterialLabel lblCapitalCol;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtCountryCapitalAD;
        private MaterialSkin.Controls.MaterialLabel lblAreaCol;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtCountryAreaAD;
        private MaterialSkin.Controls.MaterialLabel lblPopCol;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtCountryPopAD;
        private MaterialSkin.Controls.MaterialLabel lblRegionCol;
        private MaterialSkin.Controls.MaterialFlatButton btnSaveAD;
        private MaterialSkin.Controls.MaterialFlatButton btnCloseAD;
        private System.Windows.Forms.ComboBox cbCountryRegion;
        private System.Windows.Forms.Label lblNameCountry;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.Label lblCapital;
        private System.Windows.Forms.Label lblArea;
        private System.Windows.Forms.Label lblPopulation;
        private System.Windows.Forms.Label lblRegion;
        private System.Windows.Forms.Label lblCode2;
    }
}