﻿
namespace CRMGURU
{
    //строковые паттерны
    static class StringPatterns
    {
        public static string NoDigits()
        {
            const string pattern = @"^\D+$";
            return pattern;
        }

        public static string EnglishString(bool withSpaces)
        {
            string pattern = string.Empty;
            if (withSpaces) return pattern = @"^[a-zA-Z0-9 ]*$";
            else return pattern = @"^[a-zA-Z0-9]*$";
        }

        public static string DigitsOnly()
        {
            const string pattern = @"^\d+$";
            return pattern;
        }
    }
}
