﻿using System;
using System.Data;
using System.Windows.Forms;
using MaterialSkin.Controls;
using System.Net;

namespace CRMGURU
{    
    public partial class CountryInformer : MaterialForm
    {
        DataTable data;
        DbHandler dBHandler;
        
        public CountryInformer(DbHandler dbHandler)
        {
            InitializeComponent();          
            this.dBHandler = dbHandler;
            data = new DataTable();
            FormDesign.SetDesign(this);        
        }

        private void btnFindCountry_Click(object sender, EventArgs e)
        {        
            if (CountryNameCorrect(out string countryName))
            {                        
                Api api = new Api();                
                try
                {
                    string requestFilter = "?fields=name;numericCode;capital;area;population;region";
                    Country[] countries = api.SearchCountries(countryName, requestFilter);
                    DisplayData(countries);
                }
                catch (WebException)
                {                   
                    MessageBox.Show("The country was not found!", "Error code: 404",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                    FindCountryInDB(countryName);                   
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }         
        }

        //Отображает весь список стран.
        private void btnCountryList_Click(object sender, EventArgs e)
        {         
            try
            {
                dBHandler.OpenConnection();
                dBHandler.CreateCmd();
                dBHandler.ExecuteNonQuery(SqlQuery.DisplayAllCountries());

                dBHandler.FillAdapter(ref data);
                DisplayData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                dBHandler.CloseConnection();
            }
        }
            
        //Проверят правильность ввода страны.
        private bool CountryNameCorrect(out string countryName)
        {
            countryName = txtCountryName.Text;

            if (string.IsNullOrEmpty(countryName))
            {
                MessageBox.Show("Please enter a country!", "Сountry name was not entered.",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (!CheckerString.StringCorrect(countryName, StringPatterns.NoDigits()))
            {
                MessageBox.Show("A country cannot contain numbers!", "The country contained numbers.",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (!CheckerString.StringCorrect(countryName, StringPatterns.EnglishString(false)))
            {
                MessageBox.Show("The country must be in English without spaces!", "The country contained no English words.",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        //Ищет страну в базе данных.
        private void FindCountryInDB(string countryName)
        {
            try
            {               
                dBHandler.OpenConnection();
                dBHandler.CreateCmd();

                DialogResult dialogResult = MessageBox.Show("Do you want to save data to the database?",
                        "The country was not found!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dialogResult == DialogResult.Yes)
                    InvokeAddDataForm(countryName);
            }         
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);              
            }
            finally
            {
                dBHandler.CloseConnection();
            }
        }
     
        private void DisplayData() => dataGridView.DataSource = data;

        private void DisplayData(Country[] countries)
        {          
            data.Columns.Clear();
            data.Clear();

            string[] columns = new string[6] { "Name", "Code", "Capital", "Area", "Population", "Region" };

            for (int i = 0; i < columns.Length; i++)
            {
                data.Columns.Add(new DataColumn(columns[i], typeof(string)));
            }
                    
            foreach (Country country in countries)
            {             
                data.Rows.Add(country.Name, country.NumericCode,
                    country.Capital, country.Area, country.Population, country.Region);
            }

            int rowHeadersWidth = 141;
            dataGridView.RowHeadersWidth = rowHeadersWidth;
            dataGridView.ScrollBars = ScrollBars.Both;

            DisplayData();
        }
      
        private void InvokeAddDataForm(string countryName)
        {          
            AddData addDataForm = new AddData(countryName, dBHandler);
            addDataForm.ShowDialog();          
        }            
    }
}
